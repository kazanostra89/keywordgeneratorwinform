﻿namespace KeywordGenerator
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label1;
            this.comboBoxWordOne = new System.Windows.Forms.ComboBox();
            this.comboBoxWordTwo = new System.Windows.Forms.ComboBox();
            this.comboBoxWordThree = new System.Windows.Forms.ComboBox();
            this.labelKeywordGenerator = new System.Windows.Forms.Label();
            this.listBoxHistory = new System.Windows.Forms.ListBox();
            this.buttonKeywordGenerator = new System.Windows.Forms.Button();
            label2 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            label2.Location = new System.Drawing.Point(40, 176);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(124, 16);
            label2.TabIndex = 5;
            label2.Text = "История событий:";
            // 
            // comboBoxWordOne
            // 
            this.comboBoxWordOne.FormattingEnabled = true;
            this.comboBoxWordOne.Location = new System.Drawing.Point(40, 80);
            this.comboBoxWordOne.Name = "comboBoxWordOne";
            this.comboBoxWordOne.Size = new System.Drawing.Size(121, 21);
            this.comboBoxWordOne.TabIndex = 0;
            this.comboBoxWordOne.Tag = "WordOne";
            this.comboBoxWordOne.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBox_KeyPress);
            // 
            // comboBoxWordTwo
            // 
            this.comboBoxWordTwo.FormattingEnabled = true;
            this.comboBoxWordTwo.Location = new System.Drawing.Point(200, 80);
            this.comboBoxWordTwo.Name = "comboBoxWordTwo";
            this.comboBoxWordTwo.Size = new System.Drawing.Size(121, 21);
            this.comboBoxWordTwo.TabIndex = 1;
            this.comboBoxWordTwo.Tag = "WordTwo";
            this.comboBoxWordTwo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBox_KeyPress);
            // 
            // comboBoxWordThree
            // 
            this.comboBoxWordThree.FormattingEnabled = true;
            this.comboBoxWordThree.Location = new System.Drawing.Point(360, 80);
            this.comboBoxWordThree.Name = "comboBoxWordThree";
            this.comboBoxWordThree.Size = new System.Drawing.Size(121, 21);
            this.comboBoxWordThree.TabIndex = 2;
            this.comboBoxWordThree.Tag = "WordThree";
            this.comboBoxWordThree.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBox_KeyPress);
            // 
            // labelKeywordGenerator
            // 
            this.labelKeywordGenerator.AutoSize = true;
            this.labelKeywordGenerator.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelKeywordGenerator.Location = new System.Drawing.Point(64, 32);
            this.labelKeywordGenerator.Name = "labelKeywordGenerator";
            this.labelKeywordGenerator.Size = new System.Drawing.Size(63, 20);
            this.labelKeywordGenerator.TabIndex = 3;
            this.labelKeywordGenerator.Text = "Фраза:";
            // 
            // listBoxHistory
            // 
            this.listBoxHistory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listBoxHistory.FormattingEnabled = true;
            this.listBoxHistory.ItemHeight = 16;
            this.listBoxHistory.Location = new System.Drawing.Point(40, 192);
            this.listBoxHistory.Name = "listBoxHistory";
            this.listBoxHistory.Size = new System.Drawing.Size(448, 180);
            this.listBoxHistory.TabIndex = 4;
            // 
            // buttonKeywordGenerator
            // 
            this.buttonKeywordGenerator.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonKeywordGenerator.Location = new System.Drawing.Point(176, 128);
            this.buttonKeywordGenerator.Name = "buttonKeywordGenerator";
            this.buttonKeywordGenerator.Size = new System.Drawing.Size(168, 32);
            this.buttonKeywordGenerator.TabIndex = 6;
            this.buttonKeywordGenerator.Text = "Сгенерировать фразу";
            this.buttonKeywordGenerator.UseVisualStyleBackColor = true;
            this.buttonKeywordGenerator.Click += new System.EventHandler(this.buttonKeywordGenerator_Click);
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            label1.Location = new System.Drawing.Point(112, 384);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(319, 16);
            label1.TabIndex = 7;
            label1.Text = "Добавить значение: Enter; Удалить значение: \'\\\'";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(531, 402);
            this.Controls.Add(label1);
            this.Controls.Add(this.buttonKeywordGenerator);
            this.Controls.Add(label2);
            this.Controls.Add(this.listBoxHistory);
            this.Controls.Add(this.labelKeywordGenerator);
            this.Controls.Add(this.comboBoxWordThree);
            this.Controls.Add(this.comboBoxWordTwo);
            this.Controls.Add(this.comboBoxWordOne);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Генератор фраз";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxWordOne;
        private System.Windows.Forms.ComboBox comboBoxWordTwo;
        private System.Windows.Forms.ComboBox comboBoxWordThree;
        private System.Windows.Forms.Label labelKeywordGenerator;
        private System.Windows.Forms.ListBox listBoxHistory;
        private System.Windows.Forms.Button buttonKeywordGenerator;
    }
}

