﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KeywordGenerator
{
    public partial class Form1 : Form
    {
        private Random rnd;

        public Form1()
        {
            InitializeComponent();
            MyInitializedComponents();
        }

        private void MyInitializedComponents()
        {
            rnd = new Random();
        }

        private void comboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;

            string message;
            string comboBoxText = comboBox.Text;

            if (e.KeyChar == (char)Keys.Enter)
            {
                if (comboBoxText.Length > 0)
                {
                    if (!comboBox.Items.Contains(comboBoxText))
                    {
                        comboBox.Items.Add(comboBoxText);

                        message = "Добавлено в " + comboBox.Tag.ToString() + ": " + comboBoxText;

                        AddEntryToHistoryList(message);

                        comboBox.Text = String.Empty;
                    }
                    else
                    {
                        message = "Совпадение значения в " + comboBox.Tag.ToString() + ": " + comboBoxText;

                        AddEntryToHistoryList(message);
                    }
                }

                e.Handled = true;
            }
            else if (e.KeyChar == (char)Keys.RWin)
            {
                if (comboBoxText.Length > 0)
                {
                    if (comboBox.Items.Contains(comboBoxText))
                    {
                        comboBox.Items.Remove(comboBoxText);

                        message = "Удалено из " + comboBox.Tag.ToString() + ": " + comboBoxText;

                        AddEntryToHistoryList(message);

                        comboBox.Text = String.Empty;
                    }
                }

                e.Handled = true;
            }
        }


        private void AddEntryToHistoryList(string entry)
        {
            if (!listBoxHistory.Items.Contains(entry))
            {
                listBoxHistory.Items.Add(entry);
            }
        }

        private void buttonKeywordGenerator_Click(object sender, EventArgs e)
        {
            if (comboBoxWordOne.Items.Count != 0 && comboBoxWordTwo.Items.Count != 0
                && comboBoxWordThree.Items.Count !=0
                )
            {
                int numberOfAttempts = 0;
                string generatorString;

                do
                {
                    if (numberOfAttempts > 5)
                    {
                        return;
                    }

                    generatorString = "Фраза: "
                    + comboBoxWordOne.Items[rnd.Next(0, comboBoxWordOne.Items.Count)].ToString() + " "
                    + comboBoxWordTwo.Items[rnd.Next(0, comboBoxWordTwo.Items.Count)].ToString() + " "
                    + comboBoxWordThree.Items[rnd.Next(0, comboBoxWordThree.Items.Count)].ToString() + " ";

                    numberOfAttempts++;

                } while (labelKeywordGenerator.Text.Contains(generatorString));

                labelKeywordGenerator.Text = generatorString;
            }
        }






    }
}
